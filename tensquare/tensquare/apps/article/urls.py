from django.conf.urls import url
from rest_framework.routers import DefaultRouter, SimpleRouter

from article import views
from article.views import ChannelListView, MySearchView, LabelsViewSet

urlpatterns = [
    url(r'^channels/$', ChannelListView.as_view()),
    url(r'^articles/search/$', MySearchView.as_view()),
]
router = DefaultRouter()
router.register('article', views.ArticleViewSet, 'article')
urlpatterns += router.urls


# 标签
# router = SimpleRouter()  # 创建路由对象
# router.register(prefix='labels', viewset=LabelsViewSet, basename='labels')  # 注册路由
# urlpatterns += router.urls