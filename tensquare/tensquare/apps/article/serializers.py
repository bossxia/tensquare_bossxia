from rest_framework import serializers
from article.models import Channel, Article, Comment
from question.models import Label
from user.models import User


class LabelsSerializer(serializers.ModelSerializer):
    # avatar = serializers.CharField()
    class Meta:
        model = Label
        fields = ('id',
                  'label_name')



class ArticleSerializer(serializers.ModelSerializer):
    '''文章阅读规格表序列化器'''
    class Meta:
        model = Article
        fields = ('id',
                  'title')


class UserDetailSerializer(serializers.ModelSerializer):
    '''用户详情'''
    articles = ArticleSerializer(read_only=True, many=True)
    # fans = serializers.SerializerMethodField()    # User模型类中manytomany

    class Meta:
        model = User
        fields = ('id',
                  'username',
                  'avatar',
                  'articles',
                  'fans')


class ChannelSerializer(serializers.ModelSerializer):
    '''频道序列化器'''
    class Meta:
        model = Channel
        fields = '__all__'


class ArticlePublicSerializer(serializers.ModelSerializer):
    '''发布文章序列化器'''
    image = serializers.CharField(required=False, default='', allow_blank=True)

    class Meta:
        model = Article
        exclude = ('collected_users',)


class ArticleSerializerForList(serializers.ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    collected = serializers.BooleanField(default=False)

    class Meta:
        model = Article
        fields = ('id',
                  'title',
                  'content',
                  'createtime',
                  'user',
                  'collected_users',
                  'collected',
                  'image',
                  'visits'
                  )



class CommentPublicSerializer(serializers.ModelSerializer):
    '''发布评论序列化器'''
    class Meta:
        model = Comment
        fields= '__all__'


class ArticleCollectSerializer(serializers.ModelSerializer):
    '''文章收藏/取消收藏序列化器'''
    class Meta:
        model = Article
        fields = '__all__'


class Comment3SerializerList(serializers.ModelSerializer):
    user = UserDetailSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = '__all__'


class Comment2SerializerList(serializers.ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    subs = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = '__all__'

    def get_subs(self, obj):
        subs = Comment.objects.filter(parent=obj)
        return Comment3SerializerList(subs, many=True).data


class CommentSerializerList(serializers.ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    subs = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = '__all__'

    def get_subs(self, obj):
        subs = Comment.objects.filter(parent=obj)
        return Comment2SerializerList(subs, many=True).data


class ArticleDetailSerializer(serializers.ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    comments = serializers.SerializerMethodField()
    labels = serializers.SerializerMethodField()
    collected_users = serializers.SerializerMethodField()

    class Meta:
        model=Article
        fields='__all__'

    def get_labels(self, obj):
        data = obj.labels.all()
        labels_list = []
        for label in data:
            labels_list.append(label.id)
        return labels_list

    def get_collected_users(self, obj):
        data = obj.collected_users.all()
        users_list = []
        for user in data:
            users_list.append(user.username)
            return users_list

    def get_comments(self, obj):
        comments = Comment.objects.filter(article=obj).filter(parent=None)
        serializer = CommentSerializerList(comments, many=True)
        return serializer.data


class SearchSerializer(serializers.ModelSerializer):
    '''搜索文章列表序列化器'''
    class Meta:
        model = Article
        fields = '__all__'