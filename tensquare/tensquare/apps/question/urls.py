
from django.conf.urls import url
from rest_framework.routers import SimpleRouter

from question import views

urlpatterns = [
]
router = SimpleRouter()
#标签
router.register(r'labels', views.LabelsViewSet)
#问答相关
router.register(r'questions', views.QuestionViewSet)
#回答问题
router.register(r'reply', views.ReplyViewSet)
urlpatterns += router.urls