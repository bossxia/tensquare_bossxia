from django.shortcuts import render

# Create your views here.
import random
from rest_framework.views import APIView
from django_redis import get_redis_connection
from rest_framework.response import Response
from celery_tasks.sms.tasks import send_sms_code


#短信验证码
class SMSCodeView(APIView):
    def get(self, request, mobile):
        #与数据库建立链接
        redis_conn = get_redis_connection('verify_codes')
        send_flag = redis_conn.get('send_flag_%s' % mobile)

        if send_flag:
            return Response({'success': False, 'message': '请求次数过于频繁'}, status=400)
        #１、提取参数
        #２、参数校验
        #3、业务逻辑处理
        #生成短信验证码
        sms_code = '%06d' % random.randint(0, 999999)
        print('短信验证码为：%s' % sms_code)
        #实例化管道
        p1 = redis_conn.pipeline()
        #将任务添加管道
        p1.setex('send_flag_%s' % mobile, 60, 1)
        p1.setex('sms_%s' % mobile, 300, sms_code)
        #实行管道
        p1.execute()
        #使用celery发送短信验证码，让第三方容连云给手机号发送短信
        send_sms_code.delay(mobile, sms_code)
        #４、构建响应
        return Response({
            'success': True,
            'message': 'OK',
            'sms_code': sms_code
        })
