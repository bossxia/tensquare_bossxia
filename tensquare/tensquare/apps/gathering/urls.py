from django.conf.urls import url

from gathering import views

urlpatterns = [
    #展示活动列表
    url(r"^gatherings/$", views.GathersView.as_view()),
    #活动详情
    url(r"^gatherings/(?P<pk>[^/.]+)/$", views.GatherView.as_view()),
    #报名活动
    url(r"^gatherings/(?P<pk>[^/.]+)/join/$", views.GatherJoinView.as_view()),
]


