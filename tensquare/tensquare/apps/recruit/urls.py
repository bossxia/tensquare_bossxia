

from recruit import views
from rest_framework.routers import SimpleRouter
urlpatterns = []

router = SimpleRouter()
#企业
router.register(r'enterprise', views.EnterpriseViewSet)
#职位
router.register(r'recruits', views.RecruitViewSet)
#热门城市
router.register(r'city', views.CityViewSet)
urlpatterns += router.urls


