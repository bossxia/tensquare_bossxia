from rest_framework.views import exception_handler
from mongoengine import  ValidationError
from rest_framework.response import Response


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)

    if response is not None:
        return response

    if isinstance(exc, ValidationError):
        return Response(data={'message':'未登录'}, status=401)

    return None