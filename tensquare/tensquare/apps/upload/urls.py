
from django.conf.urls import url
from django.urls import path

from upload.views import *

urlpatterns = [
    #avatar
    url(r'^upload/avatar/$', ImageUploadViewForAvatar.as_view()),
    path('upload/common/',ImageUploadForCKEditorView.as_view()),
]