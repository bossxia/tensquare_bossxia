"""tensquare URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),

    #短信总路由
    url(r'', include("sms.urls")),

    #用户总路由
    url(r'', include('user.urls')),

    #招聘总路由
    url(r'', include('recruit.urls')),

    #吐槽总路由
    url(r'', include('spit.urls')),

    #活动总路由
    url(r'', include('gathering.urls')),

    #问答总路由
    url(r'', include('question.urls')),

    #头条总路由
    url(r'', include('article.urls')),

    #上传图片总路由
    url(r'', include('upload.urls')),
]
